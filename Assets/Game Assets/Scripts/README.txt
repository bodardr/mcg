Welcome to the scripts folder!

In order to understand how the game works globally, here's some insight:

- First off, I highly encourage you check out the Board Manager (BoardManagement/BoardManager.cs).
It contains information regarding the most centric piece of gameplay. 
It starts the game, holds the turn count, holds the players and calls each other's BeginTurn methods.
It also holds the Board Action Event Queue, which allows for game developement to go in an orderly manner.
This queue holds every single coroutine responsible for actions done within the game (Drawing a card, placing a card, attacking, etc...)
Note : It interacts with the BoardAnimator for procedural animations or just simple interactions with the integrated Animator.

- Then there's each individual Player. The Player contains the drawing deck, which is the deck from which cards are being drawn.
Once the player places cards by dragging a card from its hand to entity board, the card is placed.
Once placed, the player's energy is subtracted by the cost of the card, and the card's action is called (wether initializing the entity, calling the spell, etc...)
The player can be attacked by any spell or card, and will lose the game once its health reaches zero.

The Player has a human and AI counterpart, each with their specifications for artificial input or user input.

- Finally, the cards. The cards are seperatated in two, their data and behavior. Their data is stored as scriptable objects, and
the way they behave is dictated by their behavior counterpart. The base class for CardBehavior, for example, contains the basic
drag and drop features in the game. As for entities, they handle much more actions and features.

A Card Animator has been supplied for procedural animations and interaction with the Layouts

- The cards don't directly displace themselves in the hierarchy. Therefore, they are "attached" to layout elements, which are, in turn, children of layout groups.
This allows the cards to move in a smooth (lerp-like) manner. Every time a card is placed, the layout elements update themselves, and the layout group rebuilds.

- The Deck Management handles everything in the deck building section, including persistance. 
I used the card's path as a reference to the card itself, since you can use this information to pull it off directly from the Asset Bundle.

If you have any other questions, feel free to ask me, and I'll try my best to answer them.

As for the code's quality, here's my own analysis:
You will probably find that I use the board manager a lot. Actually, I do. I should've been thorough when dividing this class.

My proudest achievement when making this game was the scripable object integration. 
I think dividing data and behavior for the cards was a good idea, and allowed me to add entire sets in a matter of hours.
Plus, it has the extensibility to add custom events which can be very diverse in a rather small amount of code.