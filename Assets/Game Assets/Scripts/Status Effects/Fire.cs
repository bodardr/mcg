﻿public class Burn : StatusEffect
{
    private const int BURN_AMOUNT = 1;

    public override void Apply(CardBehavior origin, BoardManager boardManager)
    {
        (origin as EntityCardBehavior)?.TakeDamage(BURN_AMOUNT);
    }

    public override void RollbackEffect(CardBehavior origin, BoardManager boardManager)
    {
       //Nothing to do!
    }
}
