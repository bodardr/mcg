﻿public abstract class StatusEffect
{
    private int turnsLeft = 0;

    public bool IsOver => turnsLeft <= 0;

    public void DecrementTurnLeft()
    {
        --turnsLeft;
    }

    public void AddTurns(int amount)
    {
        turnsLeft += amount;
    }

    public abstract void Apply(CardBehavior origin, BoardManager boardManager);

    public abstract void RollbackEffect(CardBehavior origin, BoardManager boardManager);
}
