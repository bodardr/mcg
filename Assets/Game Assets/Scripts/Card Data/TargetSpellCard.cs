﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "New Target Spell", menuName = "Card/Target Spell", order = 1)]
public class TargetSpellCard : SpellCard
{
    public override CardBehavior Initialize(Player player, Transform parent)
    {
        GameObject cardGameObject = Instantiate((GameObject)Resources.Load(CARD_TEMPLATE_GAMEOBJECT_NAME), parent);
        cardGameObject.name = name;
        TargetSpellCardBehavior behavior = cardGameObject.AddComponent<TargetSpellCardBehavior>();
        behavior.Initialize(player, this);
        return behavior;
    }
}
