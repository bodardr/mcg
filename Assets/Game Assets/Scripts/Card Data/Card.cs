﻿using System;
using UnityEngine;

[Serializable]
public abstract class Card : ScriptableObject
{
    [Header("Storage")]
    [SerializeField]
    private bool unobtainable;

    [Header("Data")]
    [SerializeField]
    private string cardName;

    [SerializeField]
    private Sprite frontThumbnail;

    [SerializeField]
    [Range(0,10)]
    private int energyCost;

    [SerializeField]
    private AudioClip actionSound;

    [SerializeField]
    [TextArea]
    private string description;

    [SerializeField]
    private CardVisualEffect placingEffect;

    public bool Unobtainable => unobtainable;

    public string CardName => cardName;

    public Sprite FrontThumbnail => frontThumbnail;

    public int EnergyCost => energyCost;

    public AudioClip ActionSound => actionSound;

    public string Description => description;

    public CardVisualEffect PlacingEffect => placingEffect;

    public abstract CardBehavior Initialize(Player player, Transform parent);

    public abstract CardBehavior InitializeAsPreview(Transform parent);
}