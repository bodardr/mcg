﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "new SingleMorphCard", menuName = "Card/Morphing/Single Morph Card")]
public class SingleMorphCard : EntityCard
{
    [SerializeField]
    private CardCondition morphCondition;

    public CardCondition MorphCondition => morphCondition;
}
