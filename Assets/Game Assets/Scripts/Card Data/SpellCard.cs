﻿using System;
using System.IO;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "New Spell", menuName = "Card/Spell", order = 1)]
public class SpellCard : Card
{
    protected const string CARD_TEMPLATE_GAMEOBJECT_NAME = "Card Template";

    public CardEventRunnable onSpellCast = null;

    public CardEventRunnable OnSpellCast => onSpellCast;

    public override CardBehavior Initialize(Player player, Transform parent)
    {
        GameObject cardGameObject = Instantiate((GameObject)Resources.Load(CARD_TEMPLATE_GAMEOBJECT_NAME), parent);
        cardGameObject.name = name;
        SpellCardBehavior behavior = cardGameObject.AddComponent<SpellCardBehavior>();
        behavior.Initialize(player, this);
        return behavior;
    }

    public override CardBehavior InitializeAsPreview(Transform parent)
    {
        GameObject cardGameObject = Instantiate((GameObject)Resources.Load(Path.Combine("Preview", CARD_TEMPLATE_GAMEOBJECT_NAME)), parent);
        cardGameObject.name = name;

        SpellCardBehavior behavior = cardGameObject.AddComponent<SpellCardBehavior>();
        behavior.Initialize(null, this);

        return behavior;
    }
}
