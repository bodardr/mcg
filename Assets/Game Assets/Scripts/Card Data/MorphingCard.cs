﻿using System.IO;
using UnityEngine;

[CreateAssetMenu(fileName = "new MorphingCard", menuName = "Card/Morphing/Morphing Card")]
public class MorphingCard : SingleMorphCard
{
    [SerializeField]
    private SingleMorphCard[] nextMorphingStages;

    public SingleMorphCard[] NextMorphingStages => nextMorphingStages;

    public override CardBehavior Initialize(Player player, Transform parent)
    {
        GameObject cardGameObject = Instantiate((GameObject) Resources.Load(ENTITY_CARD_TEMPLATE_GAMEOBJECT_NAME), parent);
        cardGameObject.name = name;
        MorphingCardBehavior behavior = cardGameObject.AddComponent<MorphingCardBehavior>();
        behavior.Initialize(player, this);
        return behavior;
    }

    public override CardBehavior InitializeAsPreview(Transform parent)
    {
        GameObject cardGameObject = Instantiate((GameObject)Resources.Load(Path.Combine("Preview", ENTITY_CARD_TEMPLATE_GAMEOBJECT_NAME)),
            parent);
        cardGameObject.name = name;
        MorphingCardBehavior behavior = cardGameObject.AddComponent<MorphingCardBehavior>();
        behavior.Initialize(null, this);
        return behavior;
    }
}