﻿using System;
using System.IO;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "New Entity", menuName = "Card/Entity", order = 0)]
public class EntityCard : Card
{
    protected const string ENTITY_CARD_TEMPLATE_GAMEOBJECT_NAME = "Entity Card Template";

    [SerializeField]
    private bool hyped = false;

    [SerializeField]
    private bool hasTaunt = false;

    [SerializeField]
    private bool attackDisabled = false;

    [SerializeField]
    private int attackDamage = 0;

    [SerializeField]
    private int initialHealth = 0;

    [SerializeField]
    private CardEventRunnable onPlaceEvent = null;

    [SerializeField]
    private CardEventRunnable onTurnBeginEvent = null;

    [SerializeField]
    private CardEventRunnable onTurnEndEvent = null;

    [SerializeField]
    private CardEventRunnable onDeathEvent = null;

    public CardEventRunnable OnPlaceEvent => onPlaceEvent;
    public CardEventRunnable OnTurnBeginEvent => onTurnBeginEvent;
    public CardEventRunnable OnTurnEndEvent => onTurnEndEvent;
    public CardEventRunnable OnDeathEvent => onDeathEvent;

    public bool Hyped => hyped;

    public bool HasTaunt => hasTaunt;

    public int AttackDamage => attackDamage;

    public int InitialHealth => initialHealth;

    public bool AttackDisabled => attackDisabled;

    public override CardBehavior Initialize(Player player, Transform parent)
    {
        GameObject cardGameObject = Instantiate((GameObject)Resources.Load(ENTITY_CARD_TEMPLATE_GAMEOBJECT_NAME), parent);
        cardGameObject.name = name;

        EntityCardBehavior behavior = cardGameObject.AddComponent<EntityCardBehavior>();
        behavior.Initialize(player, this);

        return behavior;
    }

    public override CardBehavior InitializeAsPreview(Transform parent)
    {
        GameObject cardGameObject = Instantiate((GameObject)Resources.Load(Path.Combine("Preview", ENTITY_CARD_TEMPLATE_GAMEOBJECT_NAME)), 
            parent);
        cardGameObject.name = name;

        EntityCardBehavior behavior = cardGameObject.AddComponent<EntityCardBehavior>();
        behavior.Initialize(null, this);

        return behavior;
    }
}
