﻿using UnityEngine;

[CreateAssetMenu(fileName = "CardEndTurnCondition", menuName = "Card Conditions/Card End Turn Condition")]
public class CardEndTurnCondition : CardCondition
{    
    protected override void RemoveListeners(CardBehavior origin, BoardManager boardManager)
    {
        ((EntityCardBehavior) origin).OnCardEndTurn -= OnConditionMet;
    }

    protected override void SetupListeners(CardBehavior origin, BoardManager boardManager)
    {
        ((EntityCardBehavior) origin).OnCardEndTurn += OnConditionMet;
    }
}
