﻿using System;
using UnityEngine;


[Serializable]
public abstract class CardCondition : ScriptableObject
{
    protected CardEvent OnConditionMet;
    
    public void RemoveCallback(CardEvent callback, CardBehavior origin, BoardManager boardManager)
    {
        RemoveListeners(origin, boardManager);
        OnConditionMet = null;
    }

    public void Initialize(CardEvent callback, CardBehavior origin, BoardManager boardManager)
    {
        OnConditionMet += callback;
        SetupListeners(origin, boardManager);
    }

    protected abstract void RemoveListeners(CardBehavior origin, BoardManager boardManager);

    protected abstract void SetupListeners(CardBehavior origin, BoardManager boardManager);
}
