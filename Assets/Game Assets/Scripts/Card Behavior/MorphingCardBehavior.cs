﻿using System.Collections;
using UnityEngine;

public class MorphingCardBehavior : EntityCardBehavior
{
    private MorphingCard morphingCard;
    private CardCondition currentMorphCondition;
    private int morphIndex = -1;

    public override void Initialize(Player playerInstance, Card card)
    {
        if (card is MorphingCard morphingCard)
        {
            this.morphingCard = morphingCard;
            
            if (morphingCard.MorphCondition != null)
                currentMorphCondition = Instantiate(morphingCard.MorphCondition);
            
            base.Initialize(playerInstance, morphingCard);
        }
        else
        {
            EntityCard lastEntityCard = this.card as EntityCard;
            EntityCard newEntityCard = card as EntityCard;

            int newHealth = Health + newEntityCard.InitialHealth - lastEntityCard.InitialHealth;

            currentMorphCondition = (card as SingleMorphCard)?.MorphCondition ? Instantiate((card as SingleMorphCard)?.MorphCondition) : null;
            base.Initialize(playerInstance, card);

            Health = newHealth;
        }

    }

    protected override void InitialiseBoardEntity()
    {
        base.InitialiseBoardEntity();
        
        if(currentMorphCondition != null)
            currentMorphCondition.Initialize(MorphToNextStage, this, playerInstance.BoardManager);
    }

    public virtual IEnumerator MorphToNextStage(CardBehavior origin, BoardManager boardManager)
    {
        ++morphIndex;
        
        if (currentMorphCondition != null)
            currentMorphCondition.RemoveCallback(MorphToNextStage, this, playerInstance.BoardManager);

        if (morphingCard && morphIndex < morphingCard.NextMorphingStages.Length)
            Initialize(playerInstance, morphingCard.NextMorphingStages[morphIndex]);

        EntityCard entity = card as EntityCard;
        if (entity != null)
        {

            if (entity.OnDeathEvent != null)
                OnCardDeath -= entity.OnDeathEvent.RunEvent;

            if (entity.OnTurnBeginEvent != null)
                OnCardBeginTurn -= entity.OnTurnBeginEvent.RunEvent;

            if (entity.OnTurnEndEvent != null)
                OnCardEndTurn -= entity.OnTurnEndEvent.RunEvent;


            //todo : see if you could actually morph when it is in your hand.
            InitialiseBoardEntity();
        }

        yield return null;
    }

    public override void OnDestroy()
    {
        if (playerInstance)
        {
            currentMorphCondition?.RemoveCallback(MorphToNextStage, this, playerInstance.BoardManager);
        }

        base.OnDestroy();
    }

}
