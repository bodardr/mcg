﻿public class SpellCardBehavior : CardBehavior
{
    public override void PlaceCard()
    {
        CastSpell();
    }

    public void CastSpell()
    {
        CardEventRunnable cardEvent = ((SpellCard) card).OnSpellCast;
        if(cardEvent != null)
            playerInstance.BoardManager.EnqueueCardEvents(cardEvent.RunEvent, this);
        if (card.ActionSound)
            playerInstance.BoardManager.EnqueueAudioClip(card.ActionSound);

        cardAnimator.ClearLayout();
        
        playerInstance.BoardManager.EnqueueAction(() => cardAnimator.InitiateDeathAnimation());
    }

}
