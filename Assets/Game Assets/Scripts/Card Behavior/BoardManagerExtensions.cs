﻿public static class BoardManagerExtensions
{
    public static void EnqueueCardEvents(this BoardManager boardManager, CardEvent cardEvent, CardBehavior origin)
    {
        if (cardEvent != null)
        {
            foreach (var del in cardEvent.GetInvocationList())
            {
                var individualCardEvent = (CardEvent) del;
                boardManager.EnqueueAction(new BoardActionCardEvent(individualCardEvent, origin, boardManager));
            }
        }
    }
}