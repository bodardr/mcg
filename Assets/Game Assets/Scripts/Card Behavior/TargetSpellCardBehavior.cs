﻿using UnityEngine;

public class TargetSpellCardBehavior : CardBehavior
{
    private const string CLICKABLE_TARGET_GAMEOBJECT_NAME = "ClickableTarget";
    private EntityCardBehavior targetCard = null;
    
    public void InstantiateTarget()
    {
        CardEventRunnable cardEvent = ((SpellCard) card).OnSpellCast;

        if (cardEvent != null)
        {
            GameObject targetGameObject = Instantiate(Resources.Load<GameObject>(CLICKABLE_TARGET_GAMEOBJECT_NAME),
                GetComponentInParent<Canvas>().transform);
            targetGameObject.GetComponent<ClickableTarget>().Initialize(this);
        }
    }
    
    public void CancelCast()
    {
        cardAnimator.SnapToLayout();
    }

    public override void PlaceCard()
    {
        CastSpellOnTarget();
    }

    public void SetTarget(EntityCardBehavior entityCardBehavior)
    {
        targetCard = entityCardBehavior;
    }
    
    private void CastSpellOnTarget()
    {
        if (targetCard == null)
            Debug.LogError("Target has not been set!");
        
        CardEventRunnable cardEvent = ((SpellCard)card).OnSpellCast;

        if (cardEvent)
            playerInstance.BoardManager.EnqueueCardEvents(cardEvent.RunEvent,targetCard);

        if (card.ActionSound)
            playerInstance.BoardManager.EnqueueAudioClip(card.ActionSound);

        cardAnimator.ClearLayout();

        playerInstance.BoardManager.EnqueueAction(() => cardAnimator.InitiateDeathAnimation());
    }
}
