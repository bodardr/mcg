﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class CardBehavior : MonoBehaviour
{
    [SerializeField]
    protected Card card;

    protected Player playerInstance;

    protected CardAnimator cardAnimator;

    private int energyCost;
    public int EnergyCost
    {
        get => energyCost;
        set
        {
            energyCost = value;
            SetCardCostVisual(energyCost);
        }
    }
    public Card Card => card;
    public Player PlayerInstance => playerInstance;    

    private void Awake()
    {
        cardAnimator = GetComponent<CardAnimator>();
        cardAnimator.SetCardBehavior(this);
    }

    public virtual void Initialize(Player playerInstance, Card card)
    {
        this.playerInstance = playerInstance;
        this.card = card;
        
        EnergyCost = card.EnergyCost;
        SetCardNameVisual(this.card.CardName);
        SetCardDescriptionVisual(this.card.Description);
        SetCardImageVisual(this.card.FrontThumbnail);
    }

    public abstract void PlaceCard();
    
    private void SetCardImageVisual(Sprite newSprite)
    {
        transform.Find("Card Visual").Find("IMAGE MASK").Find("MASK").GetComponent<Image>().sprite = newSprite;
    }

    private void SetCardDescriptionVisual(string newDescription)
    {
        transform.Find("Card Visual").Find("DESCRIPTION").GetComponent<TextMeshProUGUI>().text = newDescription;
    }

    private void SetCardCostVisual(int newCost)
    {
        transform.Find("Card Visual").Find("COST").GetComponent<TextMeshProUGUI>().text = newCost <= 0? "" : newCost.ToString();
    }

    private void SetCardNameVisual(string newName)
    {
        transform.Find("Card Visual").Find("NAME").GetComponent<TextMeshProUGUI>().text = newName;
    }
}
