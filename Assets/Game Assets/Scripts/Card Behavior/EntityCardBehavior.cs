﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class EntityCardBehavior : CardBehavior, IDropHandler, IAttackable
{
    private const string DUMMY_LAYOUT_GAME_OBJECT_PREFAB_NAME = "Card Layout";
    private const string TARGET_GAMEOBJECT_TAG = "Target";
    private const string CARD_VISUAL_GAMEOBJECT_NAME = "Card Visual";

    public event CardEvent OnCardDeath;
    public event CardEvent OnCardBeginTurn;
    public event CardEvent OnCardEndTurn;

    private bool isResting = true;

    private bool attackEnabled = true;

    private int health;

    private bool killLock = false;
    private bool deathTriggered = false;

    private int attackDamage;

    private List<StatusEffect> statusEffects = new List<StatusEffect>();

    private ParticleSystem sleepingParticleEffect;
    private GameObject tauntShield;
    private string SHIELD_GAMEOBJECT_NAME;

    public EntityCardBehavior()
    {
        SHIELD_GAMEOBJECT_NAME = "Shield";
    }

    public bool OnBoard { get; private set; }
    public int MaxHealth { get; set; }

    public int Health
    {
        get => health;
        set
        {
            health = Math.Min(value, MaxHealth);
            SetCardHealthVisual(health);
        }
    }

    public int AttackDamage
    {
        get => attackDamage;
        set
        {
            attackDamage = value;
            SetCardAttackDamageVisual(value);
        }
    }

    private bool IsResting
    {
        get => isResting;
        set
        {
            isResting = value;
            UpdateRestingParticleEffect();
        }
    }

    public Transform Transform => transform;
    private bool IsDead => Health <= 0;
    public bool CanAttack => OnBoard && !IsResting && attackEnabled && !cardAnimator.AnimationLocked;

    public override void Initialize(Player playerInstance, Card card)
    {
        base.Initialize(playerInstance, card);

        EntityCard entityCard = (EntityCard) card;

        sleepingParticleEffect = transform.Find(CARD_VISUAL_GAMEOBJECT_NAME).Find("Sleeping Particle Effect")
            .GetComponent<ParticleSystem>();

        tauntShield = transform.Find(CARD_VISUAL_GAMEOBJECT_NAME).Find(SHIELD_GAMEOBJECT_NAME).gameObject;

        SetCardHealthVisual(entityCard.InitialHealth);
        SetCardAttackDamageVisual(entityCard.AttackDamage);

        if (entityCard.Hyped)
            transform.Find(CARD_VISUAL_GAMEOBJECT_NAME).Find("HYPE MARKER").gameObject.SetActive(true);
        
        if (entityCard.HasTaunt)
            tauntShield.SetActive(true);
    }

    public override void PlaceCard()
    {
        playerInstance.AddEntity(this);
        InitialiseBoardEntity();

        Transform newLayout =
            ((GameObject) Instantiate(Resources.Load(DUMMY_LAYOUT_GAME_OBJECT_PREFAB_NAME),
                playerInstance.EntityDropArea)).transform;

        cardAnimator.AssignLayout(newLayout);
    }

    public void AddStatusEffect<T>(int duration) where T : StatusEffect, new()
    {
        StatusEffect sameEffect = statusEffects.Find(x => x is T);

        if (sameEffect != null)
        {
            sameEffect.AddTurns(duration);
        }
        else
        {
            StatusEffect newEffect = new T();
            newEffect.AddTurns(duration);
            statusEffects.Add(newEffect);
        }
    }

    public void ClearStatusEffect<T>() where T : StatusEffect
    {
        StatusEffect statusEffect = statusEffects.Find(x => x is T);

        if (statusEffect != null)
            statusEffects.Remove(statusEffect);
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (CanBeAttacked(eventData))
        {
            Target target = eventData.pointerDrag.GetComponent<Target>();

            if (target.Origin != this)
                target.Origin.Attack(this);
        }
    }

    public void EnterAttackAnimation(IAttackable target)
    {
        cardAnimator.EnterAttackAnimation(target.Transform);
    }

    public bool IsAttackAnimationFinished()
    {
        return !cardAnimator.AnimationLocked;
    }

    public void ExitAttackAnimation()
    {
        UpdateRestingParticleEffect();

        cardAnimator.ExitAttackAnimation();
    }

    public virtual void DealDamageToTarget(IAttackable target)
    {
        target.TakeDamage(AttackDamage);
    }

    public void Attack(IAttackable target)
    {
        //The variable is set without its encapsulation
        //in order to avoid updating the sleeping particle effect immediately.
        if (!IsResting)
            isResting = true;
        else
            throw new Exception("Card cannot attack : it is resting!");

        if (target is Player)
            playerInstance.BoardManager.EnqueueOneSidedAttack(this, target);
        else
            playerInstance.BoardManager.EnqueueTwoSidedAttack(this, target);
    }

    public virtual void TakeDamage(int amount)
    {
        Health -= amount;

        playerInstance.BoardManager.InstantiateDamageValue(transform, amount);

        if (IsDead)
            Kill();
    }

    public void Kill()
    {
        if (!CanBeKilled())
            return;

        deathTriggered = true;

        if (OnCardDeath != null)
            playerInstance.BoardManager.EnqueueCardEvents(OnCardDeath, this);

        playerInstance.RemoveEntity(this);
    }

    public void InsertKillLock()
    {
        killLock = true;
    }

    public void RemoveKillLock()
    {
        killLock = false;

        if (IsDead)
            Kill();
    }

    public void OnBeginTurn()
    {
        IsResting = false;

        if (OnCardBeginTurn != null)
            playerInstance.BoardManager.EnqueueCardEvents(OnCardBeginTurn, this);
    }

    public void OnEndTurn()
    {
        IsResting = false;

        if (OnCardEndTurn != null)
            playerInstance.BoardManager.EnqueueCardEvents(OnCardEndTurn, this);

        UpdateStatusEffects();
    }


    public virtual void OnDestroy()
    {
        EntityCard entity = (EntityCard) card;

        if (entity.OnDeathEvent != null)
            OnCardDeath -= entity.OnDeathEvent.RunEvent;

        if (entity.OnTurnBeginEvent != null)
            OnCardBeginTurn -= entity.OnTurnBeginEvent.RunEvent;

        if (entity.OnTurnEndEvent != null)
            OnCardEndTurn -= entity.OnTurnEndEvent.RunEvent;
    }

    protected virtual void InitialiseBoardEntity()
    {
        EntityCard entity = (EntityCard) card;

        MaxHealth = entity.InitialHealth;
        Health = entity.InitialHealth;
        AttackDamage = entity.AttackDamage;

        if (card.ActionSound)
            playerInstance.BoardManager.EnqueueAudioClip(card.ActionSound);

        CardEventRunnable onPlaceEvent = entity.OnPlaceEvent;

        if (onPlaceEvent != null)
            playerInstance.BoardManager.EnqueueCardEvents(onPlaceEvent.RunEvent, this);

        if (entity.OnDeathEvent != null)
            OnCardDeath += entity.OnDeathEvent.RunEvent;

        if (entity.OnTurnBeginEvent != null)
            OnCardBeginTurn += entity.OnTurnBeginEvent.RunEvent;

        if (entity.OnTurnEndEvent != null)
            OnCardEndTurn += entity.OnTurnEndEvent.RunEvent;

        if (((EntityCard) card).AttackDisabled)
            attackEnabled = false;

        IsResting = !((EntityCard) card).Hyped;

        OnBoard = true;
    }

    private bool CanBeAttacked(PointerEventData eventData)
    {
        return OnBoard && !IsDead && !cardAnimator.AnimationLocked &&
               eventData.pointerDrag.CompareTag(TARGET_GAMEOBJECT_TAG);
    }

    private void UpdateRestingParticleEffect()
    {
        if (IsResting)
        {
            sleepingParticleEffect.Play();
        }
        else
        {
            sleepingParticleEffect.Clear();
            sleepingParticleEffect.Stop();
        }
    }

    private void UpdateStatusEffects()
    {
        statusEffects.ForEach(effect =>
        {
            effect.Apply(this, playerInstance.BoardManager);
            effect.DecrementTurnLeft();
        });

        statusEffects.FindAll(effect => effect.IsOver).ForEach(x =>
        {
            x.RollbackEffect(this, playerInstance.BoardManager);
            statusEffects.Remove(x);
        });
    }

    private bool CanBeKilled()
    {
        return !deathTriggered && !killLock;
    }

    private void SetCardHealthVisual(int newHealth)
    {
        transform.Find(CARD_VISUAL_GAMEOBJECT_NAME).Find("HEALTH").GetComponent<TextMeshProUGUI>().text =
            newHealth <= 0 ? "" : newHealth.ToString();
    }

    private void SetCardAttackDamageVisual(int newAttackDamage)
    {
        transform.Find(CARD_VISUAL_GAMEOBJECT_NAME).Find("ATTACK DAMAGE").GetComponent<TextMeshProUGUI>().text =
            newAttackDamage <= 0 ? "" : newAttackDamage.ToString();
    }
}