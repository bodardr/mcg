﻿using UnityEngine;

public class DeckContainer : MonoBehaviour
{
    private Deck playerDeck;

    public Deck PlayerDeck => playerDeck;

    private Deck adversaryDeck;

    public Deck AdversaryDeck => adversaryDeck;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }


    public void AssignDecks(Deck playerDeck, Deck adversaryDeck)
    {
        this.playerDeck = playerDeck;
        this.adversaryDeck = adversaryDeck;
    }
}
