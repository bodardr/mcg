﻿using System;
using UnityEngine;

[Serializable]
public class Deck : ScriptableObject
{
    public const int MAX_CARDS = 12;

    public string[] cardPaths;

    public string deckName;

}
