﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DeckManager : MonoBehaviour
{
    private const string DECK_DIR = "PlayerDecks";

    public void CreateDeck(string createdDeckName)
    {
        Deck newDeck = ScriptableObject.CreateInstance<Deck>();
        newDeck.deckName = createdDeckName;
        SaveDeck(newDeck);
    }

    public void SaveDeck(Deck deckToSave)
    {
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, DECK_DIR)))
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, DECK_DIR));
        }

        string file = JsonUtility.ToJson(deckToSave);
        File.WriteAllText(Path.Combine(Application.persistentDataPath, DECK_DIR, deckToSave.deckName + ".json"), file);
    }

    public Deck LoadDeck(string deckName)
    {
        Deck loadedInstance = ScriptableObject.CreateInstance<Deck>();
        JsonUtility.FromJsonOverwrite(
            File.ReadAllText(Path.Combine(Application.persistentDataPath, DECK_DIR, deckName) + ".json"), loadedInstance);
        return loadedInstance;
    }

    public string[] GetDeckNames()
    {
        List<string> allDecks = new List<string>();

        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, DECK_DIR)))
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, DECK_DIR));
            return allDecks.ToArray();
        }

        var info = new DirectoryInfo(Path.Combine(Application.persistentDataPath, DECK_DIR));
        var fileInfo = info.GetFiles();

        foreach (var file in fileInfo)
        {
            string extension = file.Extension;
            if (extension.Equals(".json"))
            {
                allDecks.Add(file.Name.Remove(file.Name.IndexOf(extension, StringComparison.Ordinal), extension.Length));
            }
        }

        return allDecks.ToArray();
    }

    public void DeleteDeck(Deck deckToDelete)
    {
        File.Delete(Path.Combine(Application.persistentDataPath, DECK_DIR, deckToDelete.deckName) + ".json");
    }
}
