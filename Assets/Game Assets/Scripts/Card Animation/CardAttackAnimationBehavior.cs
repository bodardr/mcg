﻿using UnityEngine;

public class CardAttackAnimationBehavior : StateMachineBehaviour
{
    private Vector3 previousPosition;

    private Transform assignedTarget;

    private float animationProgress = 0.0f;

    [SerializeField]
    private AnimationCurve heightCurve;

    [SerializeField]
    private AnimationCurve lerpToPositionCurve;

    private CardAnimator cardAnimator;
    private static readonly int AttackParameterHash = Animator.StringToHash("Attack");

    private const float ANIMATION_LENGTH = 0.75f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animationProgress = 0;

        if(assignedTarget == null)
            FinishAnimation(animator);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (assignedTarget != null)
        {
            if ((animationProgress += Time.deltaTime * stateInfo.speed) < ANIMATION_LENGTH)
                animator.transform.position =
                    Vector3.Lerp(previousPosition, assignedTarget.position,
                        lerpToPositionCurve.Evaluate(animationProgress / ANIMATION_LENGTH)) +
                    Vector3.back * heightCurve.Evaluate(animationProgress / ANIMATION_LENGTH);

            else
                FinishAnimation(animator);
        }
    }   

    private void FinishAnimation(Animator animator)
    {
        animator.SetBool(AttackParameterHash, false);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RemoveAssignedTarget();
        cardAnimator.UnlockAnimation();
    }

    private void RemoveAssignedTarget()
    {
        assignedTarget = null;
    }

    public void AssignAnimationInformation(CardAnimator cardAnimator, Transform animationTarget)
    {
        this.cardAnimator = cardAnimator; 
        previousPosition = cardAnimator.transform.position;
        assignedTarget = animationTarget;
    }
}
