﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// In order for cards to move freely, they aren't attached to a formal 'parent'.
/// Instead, they are attached to a CardLayout, which dynamically refreshes the
/// layout and the card placement.
/// </summary>
public class CardLayout : MonoBehaviour
{
    private Transform attachedCard;
    private CardAnimator attachedCardAnimator;

    public void SetAttachedCard(Transform attachedCard, CardAnimator attachedCardAnimator)
    {
        this.attachedCard = attachedCard;
        this.attachedCardAnimator = attachedCardAnimator;

        var parent = transform.parent;

        LayoutRebuilder.ForceRebuildLayoutImmediate(parent as RectTransform);
        var layouts = parent.GetComponentsInChildren<CardLayout>();

        foreach (var cardLayout in layouts)
            cardLayout.UpdateHoldingCardPosition();
    }

    private void UpdateHoldingCardPosition()
    {
        //The CardLayout always has one child
        if (!attachedCard)
            Debug.LogError("CardLayout isn't assigned to a card");
        else
            attachedCardAnimator.SnapToLayout();
    }

    public void RemoveLayout()
    {
        RectTransform parent = transform.parent as RectTransform;

        transform.SetParent(null);

        LayoutRebuilder.ForceRebuildLayoutImmediate(parent);

        if (parent != null)
        {
            var layouts = parent.GetComponentsInChildren<CardLayout>();

            foreach (var cardLayout in layouts)
                cardLayout.UpdateHoldingCardPosition();
        }

        Destroy(gameObject);
    }
}