﻿using UnityEngine;
using UnityEngine.EventSystems;

class EntityCardAnimator : CardAnimator
{
    private const string TARGET_GAMEOBJECT_NAME = "Target";
    public override void OnBeginDrag(PointerEventData eventData)
    {
        var entityCardBehavior = (EntityCardBehavior)cardBehavior;
        
        if (entityCardBehavior.CanAttack)
        {
            GameObject targetGameObject = Instantiate(Resources.Load<GameObject>(TARGET_GAMEOBJECT_NAME),
                GetComponentInParent<Canvas>().transform);
            targetGameObject.GetComponent<Target>().PassInOrigin(entityCardBehavior);
            eventData.pointerDrag = targetGameObject;
        }
        else if (!entityCardBehavior.OnBoard)
        {
            base.OnBeginDrag(eventData);
        }
        else
        {
            eventData.pointerDrag = null;
        }
    }
}