﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Handles the card's procedural animation.
/// </summary>
public class CardAnimator : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private const string CARD_VISUAL_TRANSFORM_NAME = "Card Visual";

    private static readonly int SelectedParameterHash = Animator.StringToHash("Selected");
    private static readonly int DeathPropertyIndex = Animator.StringToHash("Death");
    private static readonly int AttackPropertyIndex = Animator.StringToHash("Attack");

    private const float DEPTH_COEFFICIENT = 0.7f;
    private const float TRANSITION_DURATION = 1;
    private const float ROTATION_DURATION = 0.5f;
    private const float TRANSITION_SPEED = 11f;

    [SerializeField]
    private Image[] imageRenderers;

    private Animator anim;

    protected CardBehavior cardBehavior;
    private CanvasGroup canvasGroup;

    private CardAttackAnimationBehavior attackAnimationBehavior;
    private DissolveAnimationBehavior dissolveAnimationBehavior;

    private bool animationLocked = false;

    private Transform currentAttachedLayout;
    private bool lerpingToLayout = false;

    private Coroutine lerpToLayoutCoroutine;
    
    private bool isDragging;

    public bool AnimationLocked => animationLocked;
    public bool Hidden { get; set; } = false;
    
    public bool Selectable
    {
        get => canvasGroup.blocksRaycasts;
        set => canvasGroup.blocksRaycasts = value;
    }


    private Vector3 FacingVector
    {
        get
        {
            if (transform.parent)
            {
                var forward = transform.parent.forward;
                return Hidden ? -forward : forward;
            }

            return Hidden ? Vector3.forward : Vector3.back;
        }
    }

    public void Awake()
    {
        anim = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
        attackAnimationBehavior = anim.GetBehaviour<CardAttackAnimationBehavior>();
        dissolveAnimationBehavior = anim.GetBehaviour<DissolveAnimationBehavior>();
        dissolveAnimationBehavior.SetImageRenderers(imageRenderers);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Selectable)
        {
            anim.SetBool(SelectedParameterHash, true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        anim.SetBool(SelectedParameterHash, false);
    }

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
        
        if (!cardBehavior.PlayerInstance.TurnActive)
        {
            eventData.pointerDrag = null;
            return;
        }

        Selectable = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        lerpingToLayout = false;
        
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(transform.parent as RectTransform,
            eventData.position, eventData.pressEventCamera, out var globalMousePos))
        {
            var thisTransform = transform;

            Vector3 delta = Vector3.ProjectOnPlane(thisTransform.position - globalMousePos, thisTransform.parent.forward);
            thisTransform.position = Vector3.Lerp(transform.position, globalMousePos, 0.2f);
            thisTransform.rotation =
                Quaternion.LookRotation((delta.magnitude > 1 ? delta.normalized : delta) + Vector3.down * 4,
                    thisTransform.parent.up);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Selectable = true;
        isDragging = false;
        
        if (currentAttachedLayout)
            SnapToLayout();
    }

    public IEnumerator LerpBackRotation()
    {
        float lerpValue = 0;

        Quaternion desiredRotation = Quaternion.LookRotation(FacingVector, Vector3.up);

        while ((lerpValue += Time.deltaTime) < ROTATION_DURATION)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, lerpValue / ROTATION_DURATION);
            yield return new WaitForEndOfFrame();
        }

        transform.rotation = desiredRotation;
    }

    public IEnumerator LerpBackPositionToLayout()
    {
        if (lerpingToLayout)
            yield break;

        float newPositionDelta = CalculatePositionFromChildSiblingIndex();
        float lerpTime = 0;
        lerpingToLayout = true;

        while (lerpingToLayout && currentAttachedLayout && (lerpTime += Time.deltaTime) < TRANSITION_DURATION &&
               (transform.position - currentAttachedLayout.position + Vector3.up * newPositionDelta).magnitude > 0.2f)
        {
            Vector3 lerpValue = Vector3.Lerp(transform.position,
                currentAttachedLayout.position + Vector3.up * newPositionDelta, TRANSITION_SPEED * Time.deltaTime);

            transform.position = lerpValue;
            yield return new WaitForEndOfFrame();
        }

        if (currentAttachedLayout && lerpingToLayout)
            transform.position = currentAttachedLayout.position + Vector3.up * newPositionDelta;
        lerpingToLayout = false;
    }

    private float CalculatePositionFromChildSiblingIndex()
    {
        var calculatedPosition = currentAttachedLayout.GetSiblingIndex() * DEPTH_COEFFICIENT;
        return calculatedPosition;
    }

    public void AssignLayout(Transform layout)
    {
        if (currentAttachedLayout)
            ClearLayout();

        currentAttachedLayout = layout;
        layout.GetComponent<CardLayout>().SetAttachedCard(transform, this);
    }

    public void SnapToLayout()
    {
        if (isDragging)
            return;
        
        StopAllCoroutines();
        lerpingToLayout = false;

        StartCoroutine(LerpBackPositionToLayout());
        StartCoroutine(LerpBackRotation());
    }

    public void InitiateDeathAnimation()
    {
        animationLocked = true;
        anim.SetTrigger(DeathPropertyIndex);
    }

    public void EnterAttackAnimation(Transform target)
    {
        LockAnimation();
        anim.SetBool(AttackPropertyIndex, true);
        attackAnimationBehavior.AssignAnimationInformation(this, target);
    }

    public void LockAnimation()
    {
        animationLocked = true;
    }

    public void UnlockAnimation()
    {
        animationLocked = false;
    }

    public void ExitAttackAnimation()
    {
        SnapToLayout();
    }

    public void ClearLayout()
    {
        if (!currentAttachedLayout) return;

        Transform layoutParent = currentAttachedLayout.parent;
        currentAttachedLayout.GetComponent<CardLayout>().RemoveLayout();
        currentAttachedLayout = null;
    }

    public void SetCardBehavior(CardBehavior cardBehavior)
    {
        this.cardBehavior = cardBehavior;
    }
}