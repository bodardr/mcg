﻿using UnityEngine;
using Vector3 = System.Numerics.Vector3;

public interface IAttackable
{
    Transform Transform { get; }
    
    void EnterAttackAnimation(IAttackable target);
    bool IsAttackAnimationFinished();
    void ExitAttackAnimation();

    void DealDamageToTarget(IAttackable target);

    void TakeDamage(int amount);

    void Kill();

    void InsertKillLock();
    void RemoveKillLock();
}
