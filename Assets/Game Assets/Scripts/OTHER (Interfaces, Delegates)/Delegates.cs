﻿using System.Collections;

public delegate IEnumerator CardEvent(CardBehavior origin, BoardManager boardManager);