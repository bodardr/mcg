﻿using UnityEngine;

public class AdversaryAnimator : PlayerAnimator
{
    public override Vector3 GetCardFacingVector()
    {
        return -mainCamera.forward;
    }
}
