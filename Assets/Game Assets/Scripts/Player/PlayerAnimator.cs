﻿using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    [SerializeField]
    protected Transform drawingHand;

    protected Transform mainCamera;

    protected Animator anim;

    public void Awake()
    {
        anim = GetComponent<Animator>();
        mainCamera = Camera.main.transform;
    }

    public Transform GetDrawingHand()
    {
        return drawingHand;
    }

    public void PlayDrawingAnimation()
    {
        anim.SetTrigger("Draw");
    }

    public virtual Vector3 GetCardFacingVector()
    {
        return mainCamera.forward;
    }
}
