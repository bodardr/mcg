﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Adversary : Player
{
    protected override bool HideDrawnCards => true;

    public override void BeginTurn()
    {
        base.BeginTurn();
        StartCoroutine(BeginAdversaryTurn());
    }
    
    public override void PlaceCard(CardBehavior cardBehavior)
    {
        cardBehavior.GetComponent<CardAnimator>().Hidden = false;
        base.PlaceCard(cardBehavior);
    }

    private IEnumerator BeginAdversaryTurn()
    {
        yield return new WaitUntil(() => !boardManager.ActionsDispatching);

        var adversary = boardManager.GetAdversary(this);

        CardBehavior cardToDraw;
        while ((cardToDraw = handDeck.Find(CanPlaceCard)) != null)
        {
            if (cardToDraw is TargetSpellCardBehavior targetSpellCardBehavior)
            {
                targetSpellCardBehavior.SetTarget(
                    adversary.EntityDeck[Random.Range(0, adversary.EntityDeck.Count - 1)]);
            }

            PlaceCard(cardToDraw);
            yield return new WaitUntil(() => !boardManager.ActionsDispatching);
        }

        EntityCardBehavior attackingEntityCard;
        while ((attackingEntityCard = entityDeck.Find(x => x.CanAttack)) != null)
        {
            EntityCardBehavior target = adversary.EntityDeck.Find(x => x.Health == attackingEntityCard.AttackDamage);

            if (adversary.EntityDeck.Count > 0 && target == null)
                target = adversary.EntityDeck[Random.Range(0, adversary.EntityDeck.Count - 1)];

            if (target != null)
                attackingEntityCard.Attack(target);
            else
                attackingEntityCard.Attack(adversary);

            yield return new WaitUntil(() => !boardManager.ActionsDispatching);
        }

        EndTurn();
    }

    public override bool CanPlaceCard(CardBehavior cardBehavior)
    {
        if (cardBehavior is TargetSpellCardBehavior && boardManager.GetAdversary(this).EntityDeck.Count < 1)
            return false;
        
        return base.CanPlaceCard(cardBehavior);
    }
}