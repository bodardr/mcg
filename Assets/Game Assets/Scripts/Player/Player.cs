﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

/// <summary>
/// The base class for a board player.
/// Has a drawing deck, of which cards are being drawn from.
/// Has an active entity deck.
/// Can be damaged, by enemy cards attacking him.
/// </summary>
public class Player : MonoBehaviour, IAttackable
{
    public const int MAX_ENERGY = 10;
    public const int INITIAL_HEALTH = 20;

    [SerializeField]
    protected Transform entityDropArea;

    [SerializeField]
    protected Transform handArea;
    
    [FormerlySerializedAs("bruh")]
    [SerializeField]
    protected AudioClip damageSoundEffect;

    [FormerlySerializedAs("oof")]
    [SerializeField]
    protected AudioClip deathSoundEffect;
    
    public event EventHandler<CardBehavior> OnCardPlaced;
    public event Action OnBeginTurn;
    public event Action OnEndTurn;
    
    protected List<CardBehavior> handDeck = new List<CardBehavior>();
    protected List<EntityCardBehavior> entityDeck = new List<EntityCardBehavior>();
    protected BoardManager boardManager;
    
    protected int energy;
    
    private int health;
    private int maxHealth;
    
    private int currentMaxEnergy;

    private bool killLock = false;
    
    private List<Card> drawingDeck = new List<Card>();      
    public int Health
    {
        get => health;
        set
        {
            health = value;
            SetPlayerHealthVisual(value);
        }
    }

    public int MaxHealth
    {
        get => maxHealth;
        set => maxHealth = value;
    }

    public virtual int Energy
    {
        get { return energy; }
        protected set { energy = value; }
    }

    public int CurrentMaxEnergy
    {
        get => currentMaxEnergy;
        protected set => currentMaxEnergy = value;
    }

    public bool TurnActive => boardManager.ActiveTurnPlayer == this;
    public Transform Transform => transform;
    public List<EntityCardBehavior> EntityDeck => entityDeck;
    public List<CardBehavior> HandDeck => handDeck;    
    public Transform HandArea => handArea;
    public Transform EntityDropArea => entityDropArea;
    public BoardManager BoardManager => boardManager;
    protected virtual bool HideDrawnCards => false;

    public virtual void Awake()
    {
        boardManager = GameObject.Find("BOARD MANAGER").GetComponent<BoardManager>();
    }

    public virtual void Start()
    {
        Health = INITIAL_HEALTH;
        MaxHealth = INITIAL_HEALTH;
    }

    public void AddEntity(EntityCardBehavior entity)
    {
        entityDeck.Add(entity);
        OnBeginTurn += entity.OnBeginTurn;
        OnEndTurn += entity.OnEndTurn;
    }

    /// <summary>
    /// Removes and destroys entity within the entityDeck.
    /// </summary>
    /// <param name="entity"></param>
    public void RemoveEntity(EntityCardBehavior entity)
    {
        if (!entityDeck.Exists(p => p == entity))
            throw new ArgumentException("entity isn't within the list");

        OnBeginTurn -= entity.OnBeginTurn;
        OnEndTurn -= entity.OnEndTurn;

        entityDeck.Remove(entity);

        CardAnimator cardAnimator = entity.GetComponent<CardAnimator>();

        cardAnimator.ClearLayout();
        cardAnimator.InitiateDeathAnimation();
    }

    public virtual void BeginTurn()
    {
        boardManager.EnqueueAction(ReplenishEnergy);
        boardManager.EnqueueAction(DrawRandomCard);

        if (OnBeginTurn != null)
            boardManager.EnqueueAction(OnBeginTurn);
    }

    protected void ReplenishEnergy()
    {
        if (CurrentMaxEnergy < MAX_ENERGY)
        {
            ++CurrentMaxEnergy;
        }

        Energy = CurrentMaxEnergy;
    }

    public virtual void EndTurn()
    {
        if (OnEndTurn != null)
            boardManager.EnqueueAction(OnEndTurn);
        boardManager.EndCurrentTurn();
    }

    public void DrawRandomCard()
    {
        if (drawingDeck.Count > 0)
        {
            Card newCard = drawingDeck[Random.Range(0, drawingDeck.Count)];
            drawingDeck.Remove(newCard);
            DrawCard(newCard);
        }
        else
        {
            boardManager.DisplayMessage("Player is out of cards. He takes 1 damage.", MessageType.Warning);
            TakeDamage(1);
        }
    }

    public void SubtractEnergy(int amount)
    {
        if (amount > Energy)
            throw new Exception("Energy is inferior to the amount subtracted.");
        Energy -= amount;
    }


    public void EnterAttackAnimation(IAttackable target)
    {
        throw new NotImplementedException();
    }

    public bool IsAttackAnimationFinished()
    {
        return true;
    }

    public void ExitAttackAnimation()
    {
        throw new NotImplementedException();
    }

    public void DealDamageToTarget(IAttackable target)
    {
        //todo : nothing for the moment.
        //target.TakeDamage(weaponCard.Damage);
        //--weaponCard.uses;
    }

    public void TakeDamage(int amount)
    {
        Health = Mathf.Max(0, Health - amount);

        boardManager.InstantiateDamageValue(transform, amount);

        if (IsDead() && !killLock)
            Kill();
        else if (!IsDead())
            GetComponent<AudioSource>().PlayOneShot(damageSoundEffect);
    }

    public void Kill()
    {
        boardManager.SignalDefeat(this);
    }

    public void InsertKillLock()
    {
        killLock = true;
    }

    public void RemoveKillLock()
    {
        killLock = false;
        if (IsDead())
            Kill();
    }

    private bool IsDead()
    {
        return Health <= 0;
    }

    public virtual void PlaceCard(CardBehavior cardBehavior)
    {
        if (Energy < cardBehavior.EnergyCost)
            throw new Exception("Player's energy is inferior to the entityCardBehavior's energy cost");

        if (!handDeck.Exists(x => x == cardBehavior))
            throw new Exception("Card isn't in the player's deck!");

        SubtractEnergy(cardBehavior.EnergyCost);
        handDeck.Remove(cardBehavior);

        cardBehavior.GetComponent<CardAnimator>().ClearLayout();

        boardManager.EnqueueAction(new BoardActionPlaceCard(this, cardBehavior));

        OnCardPlaced?.Invoke(this, cardBehavior);
    }

    public void PlaceEntityDirectly(EntityCard card)
    {
        card.Initialize(this, boardManager.GetCardHolder()).PlaceCard();
    }

    public virtual bool CanPlaceCard(CardBehavior cardBehavior)
    {
        bool entityLimitReached = EntityDeck.Count >= BoardManager.MAX_CARD_ENTIES_ON_BOARD;

        if (entityLimitReached)
        {
            boardManager.DisplayMessage("There are too much entities on the board");
            return false;
        }

        return Energy >= cardBehavior.EnergyCost && cardBehavior.PlayerInstance == this && TurnActive &&
               !entityLimitReached;
    }

    private void SetPlayerHealthVisual(int value)
    {
        transform.Find("Panel").Find("Life Text").GetComponent<TextMeshProUGUI>().text = value.ToString("00");
    }

    public void DrawCard(Card cardToDraw)
    {
        BoardManager.EnqueueAction(new BoardActionDrawCard(boardManager, this, cardToDraw, HideDrawnCards));
    }

    public void DrawCardImmediate(Card cardToDraw)
    {
        BoardManager.EnqueueImmediateAction(new BoardActionDrawCard(boardManager, this, cardToDraw, HideDrawnCards));
    }

    public void AddCardToHand(CardBehavior drawnCardBehavior)
    {
        handDeck.Add(drawnCardBehavior);
    }

    public void AssignDeck(List<Card> deck)
    {
        drawingDeck = deck;
    }
}