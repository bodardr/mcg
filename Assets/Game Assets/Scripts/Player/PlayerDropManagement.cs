﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerDropManagement : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Player playerInstance;

    public Color denialColor;

    public Color dropColor;

    public Color defaultColor;

    // Start is called before the first frame update
    void Start()
    {
        playerInstance = GetComponent<Player>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag.CompareTag("Target"))
        {
            Target target = eventData.pointerDrag.GetComponent<Target>();

            if (CanAttack(eventData))
                target.Origin.Attack(playerInstance);
        }
    }

    private bool CanAttack(PointerEventData eventData)
    {
        //todo : this sucks. change it
        EntityCardBehavior attackingEntityCard = eventData.pointerDrag.GetComponent<Target>().Origin;

        if (attackingEntityCard.PlayerInstance == playerInstance)
            return false;

        foreach (EntityCardBehavior cardEntityObject in playerInstance.EntityDeck)
        {
            if (((EntityCard) cardEntityObject.Card).HasTaunt)
            {
                playerInstance.BoardManager.DisplayMessage(cardEntityObject.Card.CardName + " is in the way. You can only attack him", MessageType.Warning);
                return false;
            }
        }

        return true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null && eventData.pointerDrag.CompareTag("Target"))
        {
            EntityCardBehavior attackingEntityCard = eventData.pointerDrag.GetComponent<Target>().Origin;

            if (CanAttack(eventData))
                GetComponent<Image>().color = dropColor;
            else
                GetComponent<Image>().color = denialColor;
        }
        else
        {
            GetComponent<Image>().color = denialColor;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = defaultColor;
    }
}
