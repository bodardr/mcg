﻿using UnityEngine;
using UnityEngine.UI;

public class HumanPlayer : Player
{
    [SerializeField]
    private Transform endTurnButton = null;

    [SerializeField]
    private RelevancyDisplayHandler relevancyDisplayHandler = null;

    public override int Energy
    {
        get => base.Energy;
        protected set
        {
            base.Energy = value;
            relevancyDisplayHandler.UpdateDisplay(energy);
        }
    }

    public override void Start()
    {
        base.Start();

        relevancyDisplayHandler = GameObject.Find("PLAYER RELEVANCY").GetComponent<RelevancyDisplayHandler>();
    }

    public override void BeginTurn()
    {
        base.BeginTurn();

        endTurnButton.GetComponent<Button>().interactable = true;
    }

    public void DisableEndTurnButton()
    {
        endTurnButton.GetComponent<Button>().enabled = false;
    }
}
