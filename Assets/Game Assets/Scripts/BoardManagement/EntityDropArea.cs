﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// This is the area on which entities are dropped.
/// 
/// </summary>
public class EntityDropArea : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    private Player playerInstance;

    [SerializeField]
    private Color denialColor;

    [SerializeField]
    private Color dropColor;

    [SerializeField]
    private Color defaultColor;

    public void OnDrop(PointerEventData eventData)
    {
        var droppedGameObject = eventData.pointerDrag;

        if (!droppedGameObject.CompareTag("Card"))
            return; 

        var cardBehavior = droppedGameObject.GetComponent<CardBehavior>();

        if (!playerInstance.CanPlaceCard(cardBehavior)) 
            return;

        if (cardBehavior is TargetSpellCardBehavior targetSpellCardBehavior)
        {
            targetSpellCardBehavior.InstantiateTarget();
        }
        else
        {
            playerInstance.PlaceCard(cardBehavior);
        }
        
        GetComponent<Image>().color = defaultColor;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            GameObject pointerGameObject = eventData.pointerDrag;

            CardBehavior cardBehavior = pointerGameObject.GetComponent<CardBehavior>();

            if (cardBehavior != null && playerInstance.CanPlaceCard(cardBehavior))
            {
                GetComponent<Image>().color = dropColor;
            }
            else
            {
                GetComponent<Image>().color = denialColor;
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = defaultColor;
    }
}
