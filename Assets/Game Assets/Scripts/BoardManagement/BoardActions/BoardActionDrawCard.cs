﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

public class BoardActionDrawCard : IBoardAction
{
    private const string DUMMY_LAYOUT_GAME_OBJECT_PREFAB_NAME = "Card Layout";

    private BoardManager boardManager;

    private Card drawnCard;

    private Player player;

    private bool hideCard;

    public BoardActionDrawCard(BoardManager boardManager, Player player, Card drawnCard, bool hideCard = false)
    {
        this.boardManager = boardManager;
        this.drawnCard = drawnCard;
        this.player = player;
        this.hideCard = hideCard;
    }
    
    public IEnumerator ExecuteCoroutine()
    {
        CardBehavior drawnCardBehavior = drawnCard.Initialize(player, player.GetComponent<PlayerAnimator>().GetDrawingHand());
        player.GetComponent<PlayerAnimator>().PlayDrawingAnimation();

        CardAnimator cardAnimator = drawnCardBehavior.GetComponent<CardAnimator>();

        if (hideCard)
            cardAnimator.Hidden = true;
        cardAnimator.LockAnimation();

        yield return new WaitWhile(() => cardAnimator.AnimationLocked);

        player.AddCardToHand(drawnCardBehavior);

        drawnCardBehavior.transform.SetParent(boardManager.GetCardHolder());

        Transform layout = Object.Instantiate(Resources.Load<GameObject>(DUMMY_LAYOUT_GAME_OBJECT_PREFAB_NAME), player.HandArea).transform;
        cardAnimator.AssignLayout(layout);
    }
}
