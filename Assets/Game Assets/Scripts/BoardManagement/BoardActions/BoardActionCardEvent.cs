﻿using System;
using System.Collections;

public class BoardActionCardEvent : IBoardAction
{
    private CardEvent cardEvent;

    private CardBehavior origin;
    private BoardManager boardManager;

    public BoardActionCardEvent(CardEvent cardEvent, CardBehavior origin, BoardManager boardManager)
    {
        this.cardEvent = cardEvent;
        this.origin = origin;
        this.boardManager = boardManager;
    }

    public IEnumerator ExecuteCoroutine()
    {
        yield return cardEvent(origin, boardManager);
    }
}