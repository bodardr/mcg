﻿using System;
using System.Collections;
using UnityEngine;

public class BoardActionOneSidedAttack : IBoardAction
{
    private BoardManager boardManager;
    private IAttackable target;
    private IAttackable origin;

    public BoardActionOneSidedAttack(BoardManager boardManager, IAttackable origin, IAttackable target)
    {
        this.boardManager = boardManager;
        this.origin = origin;
        this.target = target;
    }

    public IEnumerator ExecuteCoroutine()
    {
        origin.InsertKillLock();
        target.InsertKillLock();

        origin.EnterAttackAnimation(target);
        yield return new WaitUntil(origin.IsAttackAnimationFinished);
        origin.ExitAttackAnimation();
        yield return new WaitForSeconds(0.1f);
        origin.DealDamageToTarget(target);

        origin.RemoveKillLock();
        target.RemoveKillLock();
    }
}
