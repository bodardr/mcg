﻿
using System.Collections;

public class BoardActionPlaceCard : IBoardAction
{
    private BoardManager boardManager;
    private CardBehavior placedCard;
    private Player player;

    public BoardActionPlaceCard(Player player, CardBehavior placedCard)
    {
        this.player = player;
        boardManager = player.BoardManager;
        this.placedCard = placedCard;
    }
    
    public virtual IEnumerator ExecuteCoroutine()
    {
        if (placedCard.Card.PlacingEffect != null)
        {
            yield return boardManager.StartCoroutine(placedCard.Card.PlacingEffect.ExecuteEffect(placedCard, boardManager));
        }
        
        placedCard.PlaceCard();
    }
}