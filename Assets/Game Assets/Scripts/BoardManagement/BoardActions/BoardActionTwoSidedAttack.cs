﻿using System;
using System.Collections;
using UnityEngine;

public class BoardActionTwoSidedAttack : IBoardAction
{
    private BoardManager boardManager;
    private IAttackable target;
    private IAttackable origin;

    public BoardActionTwoSidedAttack(BoardManager boardManager, IAttackable origin, IAttackable target)
    {
        this.boardManager = boardManager;
        this.origin = origin;
        this.target = target;
    }

    public IEnumerator ExecuteCoroutine()
    {
        origin.InsertKillLock();
        target.InsertKillLock();

        origin.EnterAttackAnimation(target);
        yield return new WaitUntil(origin.IsAttackAnimationFinished);
        origin.ExitAttackAnimation();

        origin.DealDamageToTarget(target);
        yield return new WaitForSeconds(0.1f);
        target.DealDamageToTarget(origin);

        origin.RemoveKillLock();
        target.RemoveKillLock();
    }
}
