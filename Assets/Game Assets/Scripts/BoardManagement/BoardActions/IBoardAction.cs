﻿using System;
using System.Collections;

public interface IBoardAction
{
    IEnumerator ExecuteCoroutine();
}