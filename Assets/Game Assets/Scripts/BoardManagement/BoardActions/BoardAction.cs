﻿using System;
using System.Collections;

public class BoardAction : IBoardAction
{
    private Action action;

    public BoardAction(Action action)
    {
        this.action = action;
    }

    public IEnumerator ExecuteCoroutine()
    {
        action();
        yield break;
    }
}
