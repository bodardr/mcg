﻿using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class BoardAnimator : MonoBehaviour
{
    private const string PLAYER_CURRENT_TURN_TEXT = "Your Turn";
    private const string ADVERSARY_CURRENT_TURN_TEXT = "Adversary Turn";
    private const string TURN_INDEX_TEXT = "Turn ";

    private const string VICTORY_TEXT = "Victory!";
    private const string DEFEAT_TEXT = "Defeat...";

    [SerializeField]
    private PlayableDirector director;

    [SerializeField]
    private TimelineAsset currentTurnTimeline;

    [SerializeField]
    private TimelineAsset endGameTimeline;

    [SerializeField]
    private TextMeshProUGUI endGameScreenText;

    [SerializeField]
    private TextMeshProUGUI currentTurnText;

    [SerializeField]
    private TextMeshProUGUI currentTurnIndexText;

    [SerializeField]
    private TextMeshProUGUI informationText;

    [SerializeField]
    private Color warningTextColor;

    [SerializeField]
    private Color defaultTextColor;

    public void PlayBeginTurnAnimation(int currentTurnIndex, BoardManager.PlayerIndex playerIndex)
    {
        currentTurnIndexText.text = TURN_INDEX_TEXT + currentTurnIndex / 2;
        currentTurnText.text = playerIndex == BoardManager.PlayerIndex.PLAYER
            ? PLAYER_CURRENT_TURN_TEXT
            : ADVERSARY_CURRENT_TURN_TEXT;
        director.Play(currentTurnTimeline);
    }

    internal void PlayEndScreenAnimation(bool victory)
    {
        endGameScreenText.text = victory ? VICTORY_TEXT : DEFEAT_TEXT;
        director.Stop();
        director.Play(endGameTimeline, DirectorWrapMode.Hold);
    }

    public void DisplayMessage(string message, MessageType messageType)
    {
        if (messageType == MessageType.Warning)
            informationText.color = warningTextColor;
        else
            informationText.color = defaultTextColor;

        informationText.text = message;
        informationText.GetComponent<Animator>().SetTrigger("Show");
    }
}

public enum MessageType
{
    Info,
    Warning
}
