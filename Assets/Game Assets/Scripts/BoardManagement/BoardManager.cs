﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Debug = UnityEngine.Debug;

public class BoardManager : MonoBehaviour
{
    public const int MAX_CARD_ENTIES_ON_BOARD = 8;

    private const string DAMAGE_VALUE_GAMEOBJECT_NAME = "Damage Value";
    private const int BEGINNING_DRAW_CARD_AMOUNT = 3;

    [SerializeField]
    Player playerInstance;

    [SerializeField]
    Player adversaryInstance;

    [SerializeField]
    private List<Card> playerDeck;

    [SerializeField]
    private List<Card> adversaryDeck;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private Transform cardHolder;

    [SerializeField]
    private VideoPlayer boardVideoPlayer;

    private Queue<AudioClip> audioClipsToPlay = new Queue<AudioClip>();

    private Coroutine playAllClipsCoroutine;

    private PlayerIndex currentTurn;

    private int currentTurnIndex = 1;

    private LinkedList<IBoardAction> boardActions = new LinkedList<IBoardAction>();

    private bool actionsDispatching;

    private BoardAnimator boardAnimator;

    private Transform canvas;

    private GameObject damageValuePrefab;

    private bool audioClipsPlaying = false;

    private bool gameOver = false;
    private AssetBundle cardAssetBundle;

    public bool ActionsDispatching => actionsDispatching;

    public Player ActiveTurnPlayer => currentTurn == PlayerIndex.PLAYER ? playerInstance : adversaryInstance;

    public VideoPlayer BoardVideoPlayer => boardVideoPlayer;

    public enum PlayerIndex
    {
        PLAYER = 0,
        ADVERSARY = 1
    }

    private void Awake()
    {
        damageValuePrefab = (GameObject) Resources.Load(DAMAGE_VALUE_GAMEOBJECT_NAME);
        canvas = transform.parent;
        boardAnimator = GetComponent<BoardAnimator>();
    }

    private void Start()
    {
        DeckContainer deckContainer = GameObject.Find("DECK CONTAINER")?.GetComponent<DeckContainer>();
        if (deckContainer != null)
            AssignDecks(deckContainer.PlayerDeck, deckContainer.AdversaryDeck);
        
        StartGame();
    }

    public void EndCurrentTurn()
    {
        EnqueueAction(BeginNextTurn);
    }

    public void EnqueueTwoSidedAttack(IAttackable origin, IAttackable target)
    {
        EnqueueAction(new BoardActionTwoSidedAttack(this, origin, target));
    }

    public void EnqueueOneSidedAttack(IAttackable origin, IAttackable target)
    {
        EnqueueAction(new BoardActionOneSidedAttack(this, origin, target));
    }

    public Player GetAdversary(Player player)
    {
        return playerInstance != player ? playerInstance : adversaryInstance;
    }

    public void EnqueueAudioClip(AudioClip clip)
    {
        audioClipsToPlay.Enqueue(clip);
        PlayAudioClips();
    }
    
    public void InstantiateDamageValue(Transform origin, int amount)
    {
        GameObject damageValue = Instantiate(damageValuePrefab, origin.position + damageValuePrefab.transform.position,
            damageValuePrefab.transform.rotation, origin);
        damageValue.GetComponent<DamageValue>().SetTextValue(amount);
    }

    public void EnqueueAction(IBoardAction boardAction)
    {
        boardActions.AddLast(boardAction);
        UpdateActions();
    }

    public void EnqueueAction(Action action)
    {
        boardActions.AddLast(new BoardAction(action));
        UpdateActions();
    }

    public void SignalDefeat(Player defeatedPlayer)
    {
        boardActions.Clear();
        boardAnimator.PlayEndScreenAnimation(defeatedPlayer == adversaryInstance);

        //Disables the ability to drag and drop or simply interact.
        canvas.GetComponent<GraphicRaycaster>().enabled = false;

        playerInstance.StopAllCoroutines();
        adversaryInstance.StopAllCoroutines();
    }

    public Transform GetCardHolder()
    {
        return cardHolder;
    }

    public void EnqueueImmediateAction(IBoardAction boardAction)
    {
        boardActions.AddFirst(boardAction);
        UpdateActions();
    }

    public void DisplayMessage(string message, MessageType messageType = MessageType.Info)
    {
        boardAnimator.DisplayMessage(message, messageType);
    }

    private void PlayAudioClips()
    {
        audioClipsPlaying = true;
        while (audioClipsToPlay.Count > 0)
        {
            audioSource.PlayOneShot(audioClipsToPlay.Dequeue());
        }

        audioClipsPlaying = false;
    }
    
    private void IncrementTurnIndex()
    {
        currentTurn = (PlayerIndex) (++currentTurnIndex % 2);
    }

    private void StartGame()
    {
        currentTurn = (PlayerIndex) UnityEngine.Random.Range(0, 2);

        playerInstance.AssignDeck(playerDeck);
        adversaryInstance.AssignDeck(adversaryDeck);

        for (int i = 0; i < BEGINNING_DRAW_CARD_AMOUNT; i++)
        {
            playerInstance.DrawRandomCard();
            adversaryInstance.DrawRandomCard();
        }

        BeginNextTurn();
    }

    private void BeginNextTurn()
    {
        if (gameOver)
            return;
        
        IncrementTurnIndex();

        boardAnimator.PlayBeginTurnAnimation(currentTurnIndex, currentTurn);
        switch (currentTurn)
        {
            case PlayerIndex.PLAYER:
                playerInstance.BeginTurn();
                break;

            case PlayerIndex.ADVERSARY:
                adversaryInstance.BeginTurn();
                break;
        }
    }

    private void AssignDecks(Deck playerCards, Deck adversaryCards)
    {
        if (cardAssetBundle == null)
            LoadCardAssetBundle();

        playerDeck = new List<Card>();
        adversaryDeck = new List<Card>();

        foreach (var cardPath in playerCards.cardPaths)
            playerDeck.Add(cardAssetBundle.LoadAsset<Card>(cardPath));

        foreach (var cardPath in adversaryCards.cardPaths)
            adversaryDeck.Add(cardAssetBundle.LoadAsset<Card>(cardPath));
    }

    private void LoadCardAssetBundle()
    {
        cardAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "cards"));
        if (cardAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
        }
    }

    private void OnDestroy()
    {
        if (cardAssetBundle)
            cardAssetBundle.Unload(true);
    }

    private IEnumerator DispatchActions()
    {
        while (boardActions.Count > 0)
        {
            actionsDispatching = true;
            IBoardAction nextAction = boardActions.First.Value;
            boardActions.RemoveFirst();
            yield return StartCoroutine(nextAction.ExecuteCoroutine());
        }

        actionsDispatching = false;
        yield break;
    }

    private void UpdateActions()
    {
        if (!actionsDispatching && boardActions.Count > 0)
            StartCoroutine(DispatchActions());
    }


}