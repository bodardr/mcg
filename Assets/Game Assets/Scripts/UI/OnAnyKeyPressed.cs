﻿using UnityEngine;
using UnityEngine.Events;

public class OnAnyKeyPressed : MonoBehaviour
{
    [SerializeField]
    private UnityEvent AnyKeyPressed = new UnityEvent();

    private bool anyKeyPressedInvoked = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && !anyKeyPressedInvoked)
        {
            AnyKeyPressed.Invoke();
            anyKeyPressedInvoked = true;
        }
    }
}
