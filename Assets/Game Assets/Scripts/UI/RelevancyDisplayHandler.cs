﻿using UnityEngine;
using UnityEngine.UI;

public class RelevancyDisplayHandler : MonoBehaviour
{
    [SerializeField]
    private Color displayColor;

    [SerializeField]
    private Color disabledColor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateDisplay(int energy)
    {
        for (int i = 0; i < Player.MAX_ENERGY; i++)
        {
            transform.GetChild(i).GetComponent<Image>().color = i < energy ? displayColor : disabledColor;
        }
    }
}
