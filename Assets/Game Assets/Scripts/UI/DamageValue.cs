﻿using TMPro;
using UnityEngine;

public class DamageValue : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI valueText;

    public void SetTextValue(int amount)
    {
        valueText.text = amount.ToString("-0");
    }

    public void Remove()
    {
        Destroy(gameObject);
    }

}
