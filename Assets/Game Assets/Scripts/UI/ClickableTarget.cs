﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ClickableTarget : MonoBehaviour
{
    private const float DEPTH_BIAS = 5;

    private Canvas canvas;
    private EventSystem currentEventSystem;
    
    private TargetSpellCardBehavior targetSpellCardBehavior; 

    private void Awake()
    {
        canvas = GetComponentInParent<Canvas>();
        currentEventSystem = EventSystem.current;
    }

    private void Update()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition,
            canvas.worldCamera, out var pos);

        transform.position = canvas.transform.TransformPoint(pos) + Vector3.back * DEPTH_BIAS;
        
        if (!Input.GetMouseButtonDown(0))
            return;
        
        GameObject selectedGameObject = currentEventSystem.currentSelectedGameObject;
        if (selectedGameObject)
        {
            EntityCardBehavior entityCardBehavior = selectedGameObject.GetComponent<EntityCardBehavior>();

            if (entityCardBehavior && entityCardBehavior.OnBoard)
            {
                targetSpellCardBehavior.SetTarget(entityCardBehavior);
                targetSpellCardBehavior.PlayerInstance.PlaceCard(targetSpellCardBehavior);
                Destroy(gameObject);
            }
            else
            {
                CancelTarget();
            }
        }
        else
        {
            CancelTarget();
        }
    }
    
    public void Initialize(TargetSpellCardBehavior targetSpellCardBehavior)
    {
        this.targetSpellCardBehavior = targetSpellCardBehavior;
    }
    
    private void CancelTarget()
    {
        targetSpellCardBehavior.CancelCast();
        Destroy(gameObject);
    }


}
