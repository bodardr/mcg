﻿using UnityEngine;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour
{
    [SerializeField]
    private GraphicRaycaster gameGraphicRaycaster;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip pauseAudioClip;

    private bool paused = false;

    public bool Pause
    {
        get => paused;
        set
        {
            if(value == true)
                audioSource.PlayOneShot(pauseAudioClip);

            transform.GetChild(0).gameObject.SetActive(value);
            gameGraphicRaycaster.enabled = !value;
            paused = value;
        }
    }

    public void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            TogglePause();
        }
    }

    private void TogglePause()
    {
        Pause = !Pause;
    }
}
