﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Target : MonoBehaviour, IEndDragHandler, IDragHandler
{
    private EntityCardBehavior origin;

    public EntityCardBehavior Origin => origin;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PassInOrigin(EntityCardBehavior newOrigin)
    {
        origin = newOrigin;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Destroy(gameObject);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(transform as RectTransform, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            transform.position = globalMousePos;
        }
    }
}
