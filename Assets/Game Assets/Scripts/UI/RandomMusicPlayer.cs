﻿using UnityEngine;

public class RandomMusicPlayer : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] musics;


    public void Start()
    {
        GetComponent<AudioSource>().clip = musics[Random.Range(0, musics.Length)];
        GetComponent<AudioSource>().Play();
    }
}
