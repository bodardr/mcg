﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    private Animator anim;

    [SerializeField]
    private FadeType fadeType;

    private GameObject deckContainer;

    public void Awake()
    {
        anim = GetComponent<Animator>();
        deckContainer = GameObject.Find("DECK CONTAINER");
    }

    public void LoadScene(int sceneType)
    {
        anim.Play("FadeIn" + fadeType.ToString());
        StartCoroutine(LoadSceneOnAnimationFinished((SceneType)sceneType));
    }

    public void ExitGame()
    {
        anim.Play("FadeIn" + fadeType.ToString());
        StartCoroutine(ExitOnAnimationFinished());
    }

    private IEnumerator ExitOnAnimationFinished()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
        Application.Quit(0);
    }

    private IEnumerator LoadSceneOnAnimationFinished(SceneType sceneType)
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
        SceneManager.LoadScene(sceneType.ToString());
    }

    public void LoadMainGame(Deck playerDeck, Deck adversaryDeck)
    {
        anim.Play("FadeIn" + fadeType.ToString());
        StartCoroutine(LoadGameOnAnimationFinished(playerDeck, adversaryDeck));
    }

    private IEnumerator LoadGameOnAnimationFinished(Deck playerDeck, Deck adversaryDeck)
    {
        deckContainer.GetComponent<DeckContainer>().AssignDecks(playerDeck, adversaryDeck);
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
        SceneManager.LoadScene(SceneType.Game.ToString());
    }
}

public enum SceneType
{
    TitleScreen = 0,
    MainMenu = 1,
    DeckBuilder = 2,
    Game = 3,
}

public enum FadeType
{
    Black,
    White
}
