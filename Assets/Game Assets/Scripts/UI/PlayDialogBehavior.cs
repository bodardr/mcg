﻿using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class PlayDialogBehavior : MonoBehaviour
{
    private AssetBundle presetDecksBundle;
    private Deck[] presetDecks;

    [SerializeField] private DeckManager deckManager;

    [SerializeField] private TMP_Dropdown playerDropdown;

    [SerializeField] private TMP_Dropdown adversaryDropdown;

    [SerializeField] private GameSceneManager gameSceneManager;

    private string[] deckNames;

    public void Awake()
    {
        deckNames = deckManager.GetDeckNames();

        presetDecksBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "deckpresets"));
        if (presetDecksBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }
        TextAsset[] presetDecksJson = presetDecksBundle.LoadAllAssets<TextAsset>();

        presetDecks = new Deck[presetDecksJson.Length];

        for (int i = 0; i < presetDecksJson.Length; i++)
        {
            Deck newDeck = ScriptableObject.CreateInstance<Deck>();
            JsonUtility.FromJsonOverwrite(presetDecksJson[i].text, newDeck);
            presetDecks[i] = newDeck;
        }

        List<TMP_Dropdown.OptionData> optionData = new List<TMP_Dropdown.OptionData>();
        foreach (string deckName in deckNames)
        {
            optionData.Add(new TMP_Dropdown.OptionData(deckName));
        }

        playerDropdown.AddOptions(optionData);
        adversaryDropdown.AddOptions(optionData);
    }

    public void PlayWithSelectedDecks()
    {
        Deck playerDeck;
        Deck adversaryDeck;

        playerDeck = ObtainDeckFromChoice(playerDropdown);
        adversaryDeck = ObtainDeckFromChoice(adversaryDropdown);

        gameSceneManager.LoadMainGame(playerDeck, adversaryDeck);
    }

    private Deck ObtainDeckFromChoice(TMP_Dropdown dropdownMenu)
    {
        if (dropdownMenu.value == 0)
            return presetDecks[Random.Range(0, presetDecks.Length - 1)];

        return deckManager.LoadDeck(dropdownMenu.options[dropdownMenu.value].text);
    }

    private void OnDestroy()
    {
        presetDecksBundle.Unload(true);
    }
}
