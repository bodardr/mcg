﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventProjectile : MonoBehaviour
{
    [SerializeField]
    private float TRANSITION_TIME = 2f;

    [SerializeField]
    private AnimationCurve heightCurve;

    [SerializeField]
    private float heightCurveAmplitude = 10;

    [SerializeField]
    private GameObject explosionParticleEffect;
    
    public EntityCardBehavior Target { get; set; }

    private Action<EntityCardBehavior> triggeredEvent;
    
    public void Initialize(Action<EntityCardBehavior> projectileAction)
    {
        triggeredEvent += projectileAction;
        StartCoroutine(ProjectileCoroutine());
    }

    private IEnumerator ProjectileCoroutine()
    {
        Vector3 initialPosition = transform.position;
        Vector3 targetPosition = Target.transform.position;
        
        float currentProjectileTime = 0f;
        while ((currentProjectileTime += Time.deltaTime) < TRANSITION_TIME)
        {
            Transform thisTransform = transform;
            
            thisTransform.position = Vector3.Lerp(initialPosition, targetPosition, currentProjectileTime / TRANSITION_TIME) + 
                                 heightCurve.Evaluate(currentProjectileTime / TRANSITION_TIME) * heightCurveAmplitude * thisTransform.up;
            yield return null;
        }

        triggeredEvent.Invoke(Target);
        
        if (explosionParticleEffect != null)
        {
            InstantiateParticleExplosionEffect();
        }

        Destroy(gameObject);
    }

    private void InstantiateParticleExplosionEffect()
    {
        GameObject explosionGameObject = Instantiate(explosionParticleEffect, transform.position,
            explosionParticleEffect.transform.rotation);
        Destroy(explosionGameObject, explosionGameObject.GetComponent<ParticleSystem>().main.duration);
    }
}