﻿using System.Collections;
using UnityEngine;

public abstract class CardEventRunnable : ScriptableObject
{
    public abstract IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager);
}
