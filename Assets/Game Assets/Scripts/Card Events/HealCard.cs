﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Healing Event", menuName =  "Card Events/Heal Card Event")]
public class HealCard : CardEventRunnable
{
    [SerializeField]
    private int healing;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        ((EntityCardBehavior) origin).Health += healing;
        yield return null;
    }
}
