﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "Change Card Stats", menuName = "Card Events/Change Card Stats")]
public class ChangeCardStats : CardEventRunnable
{
    [SerializeField]
    private bool allAllies = false;

    [SerializeField]
    private bool allEnemies = false;

    [SerializeField]
    private bool excludeSelf = false;

    [SerializeField]
    private int newAttackDamage;

    [SerializeField]
    private int newHealth;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        if (allAllies)
        {
            foreach (EntityCardBehavior entityObject in origin.PlayerInstance.EntityDeck)
            {
                if (!excludeSelf || excludeSelf && entityObject != origin)
                    AdjustCardStats(entityObject);
            }
        }
        else
        {
            //The event always changes its own stats if it's an entity.
            if (!excludeSelf && origin is EntityCardBehavior o)
                AdjustCardStats(o);
        }

        if (allEnemies)
        {
            foreach (EntityCardBehavior entityObject in boardManager.GetAdversary(origin.PlayerInstance).EntityDeck)
            {
                AdjustCardStats(entityObject);
            }
        }

        yield return null;
    }

    private void AdjustCardStats(EntityCardBehavior behavior)
    {
        if (newAttackDamage > 0)
            behavior.AttackDamage = newAttackDamage;

        if (newHealth > 0)
            behavior.Health = newHealth;
    }
}
