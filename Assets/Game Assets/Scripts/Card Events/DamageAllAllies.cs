﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Damage All Allies", menuName = "Card Events/Damage All Allies")]
public class DamageAllAllies : CardEventRunnable
{
    [SerializeField]
    private int explosionDamage;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        List<EntityCardBehavior> playerDeck = origin.PlayerInstance.EntityDeck;
        for (int i = playerDeck.Count - 1; i >= 0; i--)
        {
            if(playerDeck[i] != origin)
                playerDeck[i].TakeDamage(explosionDamage);
        }
        
        yield return null;
    }
}
