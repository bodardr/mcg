﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Healing Player vent", menuName =  "Card Events/Heal Player Event")]
public class HealPlayer : CardEventRunnable
{
    [SerializeField]
    private int healing;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        origin.PlayerInstance.Health += healing;
        yield return null;
    }
}
