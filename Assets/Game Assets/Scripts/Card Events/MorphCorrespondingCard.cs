﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Morph Corresponding Card", menuName = "Card Events/Morph Corresponding Card")]
public class MorphCorrespondingCard : CardEventRunnable
{
    [SerializeField]
    private MorphingCard correspondingCard;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        List<EntityCardBehavior> entityDeck =
            origin.PlayerInstance.EntityDeck.FindAll(x => x.Card == correspondingCard);
        
        foreach (var entityCardBehavior in entityDeck)
        {
            var morphingCardBehavior = (MorphingCardBehavior) entityCardBehavior;
            yield return morphingCardBehavior.MorphToNextStage(morphingCardBehavior, boardManager);
        }
        yield return null;
    }
}
