﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Draw From List", menuName = "Card Events/Draw From List")]
public class DrawFromList : CardEventRunnable
{
    [SerializeField]
    private List<Card> drawList = null;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        if (drawList.Count > 0)
            origin.PlayerInstance.DrawCardImmediate(drawList[Random.Range(0, drawList.Count)]);
        
        yield return null;
    }
}
