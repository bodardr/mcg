﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "Draw Opponent Card", menuName = "Card Events/Draw Opponent Card")]
public class PlaceCardEvent : CardEventRunnable
{
    [SerializeField]
    private EntityCard drawnEntity;

    [SerializeField]
    private bool alliedSide = false;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        Player adversary = alliedSide? boardManager.ActiveTurnPlayer : boardManager.GetAdversary(boardManager.ActiveTurnPlayer);
        adversary.PlaceEntityDirectly(drawnEntity);
        yield return null;
    }
}
