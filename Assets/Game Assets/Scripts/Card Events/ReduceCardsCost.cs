﻿using System;
using System.Collections;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Reduce Cards Cost Event",menuName = "Card Events/Reduce Cards Cost", order = 0)]
public class ReduceCardsCost : CardEventRunnable
{
    [SerializeField]
    private int costReducingAmount;

    public ReduceCardsCost(int costReducingAmount)
    {
        this.costReducingAmount = costReducingAmount;
    }

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        foreach (CardBehavior cardObject in origin.PlayerInstance.HandDeck)
        {
            --cardObject.EnergyCost;
        }
        
        yield return null;
    }
}
