﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Increase Random Card Stats", menuName = "Card Events/Increase Random Card Stats")]
public class IncreaseCardStats : CardEventRunnable
{
    [SerializeField]
    private int healthIncreased;

    [SerializeField]
    private int attackIncreased;
    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        var entity = (EntityCardBehavior) origin;

        entity.MaxHealth += healthIncreased;
        entity.Health += healthIncreased;
        entity.AttackDamage += attackIncreased;
        
        yield return null;
    }
}
