﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "New Card Event Projectile", menuName = "Card Events/Card Event Projectile")]
public class CardEventProjectileOnEntities : CardEventRunnable
{
    [FormerlySerializedAs("amountOfProjectiles")]
    [Header("Targeting")]
    [SerializeField]
    private int projectileAmount = 1;

    [SerializeField]
    private bool distinctEntities;

    [SerializeField]
    private bool allEntities;

    [SerializeField]
    private EntityType entityType;

    [Header("Event")]
    [SerializeField]
    private CardEventRunnable cardEventToExecute;

    [SerializeField]
    private GameObject projectilePrefab;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        Player adversary = boardManager.GetAdversary(origin.PlayerInstance);

        List<EntityCardBehavior> hitTargets = new List<EntityCardBehavior>();

        int currentProjectileCount = 0;
        EntityCardBehavior target;
        
        while((target = DetermineNextTarget(hitTargets, origin, boardManager)) != null && (currentProjectileCount++ < projectileAmount || allEntities))
        {
            hitTargets.Add(target);
            
            if (adversary.EntityDeck.Count <= 0)
                yield break;

            GameObject instantiatedProjectile = Instantiate(projectilePrefab, origin.transform.position,
                projectilePrefab.transform.rotation);

            var eventProjectile = instantiatedProjectile.GetComponent<EventProjectile>();

            var projectileFinishedAction = false;

            eventProjectile.Target = target;
            eventProjectile.Initialize(x =>
            {
                target.StartCoroutine(cardEventToExecute.RunEvent(target, boardManager));
                projectileFinishedAction = true;
            });

            yield return new WaitUntil(() => projectileFinishedAction);
        }
    }

    private EntityCardBehavior DetermineNextTarget(List<EntityCardBehavior> hitTargets, CardBehavior origin,
        BoardManager boardManager)
    {
        List<EntityCardBehavior> inputList;

        switch (entityType)
        {
            default:
                inputList = origin.PlayerInstance.EntityDeck;
                break;
            case EntityType.ENEMY:
                inputList = boardManager.GetAdversary(origin.PlayerInstance).EntityDeck;
                break;
            case EntityType.BOTH:
                inputList = new List<EntityCardBehavior>(origin.PlayerInstance.EntityDeck);
                inputList.AddRange(boardManager.GetAdversary(origin.PlayerInstance).EntityDeck);
                break;
        }

        System.Random rnd = new System.Random();

        if (distinctEntities || allEntities)
        {
            inputList = inputList.Except(hitTargets).ToList();
        }

        return inputList.Count > 0? inputList[rnd.Next(inputList.Count - 1)] : null;
    }

    private enum EntityType
    {
        ALLY,
        ENEMY,
        BOTH
    }
}