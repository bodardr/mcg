﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Damage Card", menuName =  "Card Events/Damage Card Event")]
public class DamageCard : CardEventRunnable
{
    [SerializeField]
    private int damage;
    
    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        ((EntityCardBehavior)origin).TakeDamage(damage);
        yield return null;
    }
}
