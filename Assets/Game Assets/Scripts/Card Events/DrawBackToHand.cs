﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "Draw Back To Hand", menuName = "Card Events/Draw Back To Hand")]
public class DrawBackToHand : CardEventRunnable
{
    [SerializeField]
    private bool activePlayerHand = false;

    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        //Draw card back to hand.
        if (activePlayerHand)
            boardManager.ActiveTurnPlayer.DrawCard(origin.Card);
        else
            origin.PlayerInstance.DrawCardImmediate(origin.Card);

        //Destroy the original card.
        (origin as EntityCardBehavior)?.Kill();
        
        yield return null;
    }
}
