﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "Death Event", menuName = "Card Events/Death Event")]
public class DeathEvent : CardEventRunnable
{
    public override IEnumerator RunEvent(CardBehavior origin, BoardManager boardManager)
    {
        if (origin is IAttackable attackable)
            attackable.Kill();
        
        yield return null;
    }
}
