﻿using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class DeckBuilder : MonoBehaviour
{
    private AssetBundle cardAssetBundle;
    public AssetBundle CardAssetBundle => cardAssetBundle;

    private DeckManager deckManager;

    [SerializeField]
    private Transform cardSelector;

    [SerializeField]
    private TMP_InputField deckNameInputField;

    [SerializeField]
    private TMP_Dropdown deckSelectionDropdown;

    [SerializeField]
    private DeckDropBehavior deckDropBehavior;

    [SerializeField]
    private TextMeshProUGUI nameTakenWarning;

    [SerializeField]
    private GameObject newDeckDialog;

    private Card[] loadedCards;
    private string[] deckNames;

    private Deck currentSelectedDeck;

    public Deck CurrentSelectedDeck
    {
        get => currentSelectedDeck;
        set
        {
            currentSelectedDeck = value;
            UpdateCurrentSelectedDeck();
        }
    }

    private void UpdateCurrentSelectedDeck()
    {
        deckDropBehavior.ClearCurrentCards();

        if (CurrentSelectedDeck.cardPaths != null && CurrentSelectedDeck.cardPaths.Length > 0)
            deckDropBehavior.LoadCardsFromDeck(CurrentSelectedDeck);
    }

    void Awake()
    {
        deckManager = GetComponent<DeckManager>();
        cardAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "cards"));
        if (cardAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }

        string[] cardsPath = cardAssetBundle.GetAllAssetNames();

        InitialiseCards(cardsPath);

        deckNames = deckManager.GetDeckNames();

        deckSelectionDropdown.onValueChanged.AddListener(SetCurrentSelectedDeck);
        InitializeDropdownMenu();
    }

    private void InitializeDropdownMenu()
    {
        UpdateDropdownOptions(deckNames);

        if (deckNames != null && deckNames.Length > 0)
        {
            deckSelectionDropdown.value = 0;
            SetCurrentSelectedDeck(0);
        }
        else
        {
            deckDropBehavior.ClearCurrentCards();
        }
    }

    private void InitialiseCards(string[] paths)
    {
        foreach (string path in paths)
        {
            Card newCard = cardAssetBundle.LoadAsset<Card>(path);

            if (!newCard.Unobtainable)
            {
                Debug.Log(newCard.CardName);
                CardBehavior cardBehavior = newCard.InitializeAsPreview(cardSelector);
                cardBehavior.enabled = false;
                CardPreviewBehavior cardPreviewBehavior = cardBehavior.gameObject.AddComponent<CardPreviewBehavior>();
                cardPreviewBehavior.AssignPath(path);
            }
        }
    }

    public void CreateDeckFromInputName()
    {
        bool deckNameValid = true;
        foreach (string deckName in deckNames)
        {
            if (deckNameInputField.text.Equals(deckName))
            {
                deckNameValid = false;
                break;
            }
        }

        if (!deckNameValid)
        {
            nameTakenWarning.enabled = true;
            return;
        }
        else
        {
            nameTakenWarning.enabled = false;
            newDeckDialog.SetActive(false);
        }

        deckManager.CreateDeck(deckNameInputField.text);

        deckNames = deckManager.GetDeckNames();

        UpdateDropdownOptions(deckNames);
        CurrentSelectedDeck = deckManager.LoadDeck(deckNameInputField.text);
        deckSelectionDropdown.SetValueWithoutNotify(deckSelectionDropdown.options.Count - 1);
    }

    private void UpdateDropdownOptions(string[] names)
    {
        List<TMP_Dropdown.OptionData> optionData = new List<TMP_Dropdown.OptionData>(names.Length);

        foreach (string deckName in names)
        {
            optionData.Add(new TMP_Dropdown.OptionData(deckName));
        }

        deckSelectionDropdown.options = optionData;
    }

    public void AssignCardsToSelectedDeck(List<string> cardPaths)
    {
        currentSelectedDeck.cardPaths = cardPaths.ToArray();
        deckManager.SaveDeck(currentSelectedDeck);
    }

    public void SaveCurrentSelectedDeck()
    {
        deckManager.SaveDeck(currentSelectedDeck);
    }

    public void DeleteCurrentSelectedDeck()
    {
        deckManager.DeleteDeck(currentSelectedDeck);
        deckNames = deckManager.GetDeckNames();
        currentSelectedDeck = null;
        InitializeDropdownMenu();
    }

    public void SetCurrentSelectedDeck(int dropdownMenuIndex)
    {
        CurrentSelectedDeck = deckManager.LoadDeck(deckSelectionDropdown.options[dropdownMenuIndex].text);
    }

    private void OnDestroy()
    {
        cardAssetBundle.Unload(true);
    }
}
