﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DeckDropBehavior : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler
{
    [SerializeField]
    private Transform deckDisplay;

    [SerializeField]
    private DeckBuilder deckBuilder;

    private Color initialColor;

    [SerializeField]
    private Color dropColor;

    [SerializeField]
    private Color denialColor;

    private int cardCount;

    public int CardCount
    {
        get => cardCount;
        private set
        {
            cardCount = value;
            cardCountText.text = "Cards : " + cardCount + " / " + Deck.MAX_CARDS;
        }
    }

    [SerializeField] private TextMeshProUGUI cardCountText;

    public void Awake()
    {
        initialColor = GetComponent<Image>().color;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if(CanAddCard(eventData))
        {
            eventData.pointerDrag.GetComponent<CardPreviewBehavior>().AssignToDeckDisplay(deckDisplay);
            ++CardCount;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag?.GetComponent<CardPreviewBehavior>() != null)
        {
            if (CanAddCard(eventData))
                GetComponent<Image>().color = dropColor;
            else
                GetComponent<Image>().color = denialColor;
        }
    }

    private bool CanAddCard(PointerEventData eventData)
    {
        return eventData.pointerDrag.GetComponent<CardPreviewBehavior>() != null && deckBuilder.CurrentSelectedDeck != null && 
               CardCount < Deck.MAX_CARDS;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = initialColor;
    }

    public void ClearCurrentCards()
    {
        foreach (Transform child in deckDisplay)
        {
            Destroy(child.gameObject);
        }

        CardCount = 0;
    }

    public void LoadCardsFromDeck(Deck deck)
    {

        foreach (string cardPath in deck.cardPaths)
        {
            Card newCard = deckBuilder.CardAssetBundle.LoadAsset<Card>(cardPath);
            CardBehavior cardBehavior = newCard.InitializeAsPreview(deckDisplay);
            cardBehavior.enabled = false;
            cardBehavior.gameObject.AddComponent<CardPreviewBehavior>().AssignToDeckDisplay(deckDisplay);
            cardBehavior.GetComponent<CardPreviewBehavior>().AssignPath(cardPath);
        }

        CardCount = deck.cardPaths.Length;
    }

    public void SaveCurrentCardsToDeck()
    {
        if (deckBuilder.CurrentSelectedDeck == null)
            return;

        List<string> cardPaths = new List<string>();
        foreach (Transform child in deckDisplay)
        {
            string cardPath = child.GetComponent<CardPreviewBehavior>().CardPath;
            Debug.Log("CardPath : " + cardPath);
            cardPaths.Add(cardPath);
        }

        deckBuilder.AssignCardsToSelectedDeck(cardPaths);
    }

    public void RemoveCardFromDeck(Transform card)
    {
        card.SetParent(GetComponentInParent<Canvas>().transform);
        --CardCount;
    }
}
