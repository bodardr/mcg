﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CardPreviewBehavior : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private bool assignedToDeckDisplay = false;
    private string cardPath;
    private CardAnimator cardAnimator;
    
    public string CardPath => cardPath;

    private const float DEPTH_COEFFICIENT = 25;

    private void Awake()
    {
        cardAnimator = GetComponent<CardAnimator>();
    }

    public virtual void OnBeginDrag(PointerEventData eventData)
    {

        if (!assignedToDeckDisplay)
        {
            CardBehavior previewCopy = GetComponent<CardBehavior>().Card
                .InitializeAsPreview(GetComponentInParent<Canvas>().transform);

            previewCopy.gameObject.AddComponent<CardPreviewBehavior>().AssignPath(cardPath);
            previewCopy.transform.position = transform.position;
            previewCopy.enabled = false;
            previewCopy.GetComponent<CardAnimator>().Selectable = false;

            eventData.pointerDrag = previewCopy.gameObject;
        }
        else
        {
            transform.GetComponentInParent<DeckDropBehavior>().RemoveCardFromDeck(transform);
            GetComponent<CardAnimator>().Selectable = false;
            assignedToDeckDisplay = false;
        }
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(transform.parent as RectTransform,
            eventData.position, eventData.pressEventCamera, out var globalMousePos))
        {
            Vector3 delta = Vector3.ProjectOnPlane(transform.position - globalMousePos, transform.parent.forward);
            transform.position = Vector3.Lerp(transform.position, globalMousePos, 0.2f);
            transform.rotation =
                Quaternion.LookRotation((delta.magnitude > 1 ? delta.normalized : delta) + Vector3.forward * 4,
                    transform.parent.up);
        }

        Debug.Log("Dragging...");
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        if (!assignedToDeckDisplay)
            Destroy(gameObject);
        else
        {
            StartCoroutine(GetComponent<CardAnimator>().LerpBackRotation());
            cardAnimator.Selectable = true;
        }
    }

    public void AssignToDeckDisplay(Transform deckDisplay)
    {
        assignedToDeckDisplay = true;
        transform.SetParent(deckDisplay);
    }

    public void AssignPath(string path)
    {
        cardPath = path;
    }
}
