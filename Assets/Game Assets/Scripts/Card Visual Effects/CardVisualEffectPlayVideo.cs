﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(fileName = "New Video Clip Effect", menuName = "Card Visual Effects/Video Clip Effect")]
public class CardVisualEffectPlayVideo : CardVisualEffect
{
    [SerializeField] 
    private VideoClip videoClip;

    public override IEnumerator ExecuteEffect(CardBehavior placedCard, BoardManager boardManager)
    {
        VideoPlayer videoPlayer = boardManager.BoardVideoPlayer;
        videoPlayer.clip = videoClip;
        videoPlayer.Prepare();
        yield return new WaitUntil(() => videoPlayer.isPrepared);
        videoPlayer.Play();
        yield return new WaitWhile(() => videoPlayer.isPlaying);
        videoPlayer.Stop();
    }
}
