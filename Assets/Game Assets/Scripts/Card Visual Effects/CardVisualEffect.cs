﻿using System.Collections;
using UnityEngine;


public abstract class CardVisualEffect : ScriptableObject
{
    public abstract IEnumerator ExecuteEffect(CardBehavior placedCard, BoardManager boardManager);
}